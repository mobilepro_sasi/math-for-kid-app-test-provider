class Question {
  final String question;
  final List<Answer> answerList;

  Question(this.question, this.answerList);
}

class Answer {
  final String answer;
  final bool isCorrect;

  Answer(this.answer, this.isCorrect);
}

List<Question> getQuestions() {
  List<Question> list = [];
  //ADD question and answer here

  list.add(Question("16 + 10 = ?", [
    Answer("14", false),
    Answer("32", false),
    Answer("26", true),
    Answer("7", false),
  ]));
  list.add(Question("6 + 5 = ?", [
    Answer("65", false),
    Answer("11", true),
    Answer("14", false),
    Answer("27", false),
  ]));
  list.add(Question("19 - 4 = ?", [
    Answer("15", true),
    Answer("11", false),
    Answer("23", false),
    Answer("10", false),
  ]));
  list.add(Question("22 - 4 = ?", [
    Answer("19", false),
    Answer("7", false),
    Answer("18", true),
    Answer("26", false),
  ]));
  list.add(Question("3 x 9 = ?", [
    Answer("21", false),
    Answer("6", false),
    Answer("36", false),
    Answer("27", true),
  ]));

  return list;
}
