import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'numQuiz.dart';

class boardGame extends StatelessWidget {
  const boardGame({super.key});

  @override
  Widget build(BuildContext context) {
    final NumQuizCounting numQuiz = Provider.of<NumQuizCounting>(context);
    return
            Text(
              "NO. ${numQuiz.number}",
              style: const TextStyle(
                fontSize: 46.0,
                color: Colors.black,
                fontFamily: "Satisfy",
              ),
            );
            // Padding(
            //   padding: const EdgeInsets.only(top: 0.0, left: 10.0),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //     children: [
            //       Text(
            //         "TIME: $seconds",
            //         style: const TextStyle(fontSize: 22, color: Colors.black),
            //       ),
            //       Text(
            //         "SCORE: $score",
            //         style: const TextStyle(fontSize: 22, color: Colors.black),
            //       ),
            //     ],
            //   ),
            // ),
            // _questionWidget(),
            // _answerList(),
            // _nextButton()

  }
}

Widget Board(context) {
  final NumQuizCounting numQuiz = Provider.of<NumQuizCounting>(context);
  return Scaffold(
    backgroundColor: Colors.white,
    appBar: AppBar(
      title: const Text('Math for kids'),
      backgroundColor: Colors.cyan[400],
    ),
    body: Center(
        child: Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(children: [
          Text(
            "NO. ${numQuiz.number}",
            style: const TextStyle(
              fontSize: 46.0,
              color: Colors.black,
              fontFamily: "Satisfy",
            ),
          ),
        ]),
      ],
    )),
  );
}
