import 'dart:async';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:math_for_kids/numQuiz.dart';
import 'package:provider/provider.dart';

import 'Quiz.dart';
import 'boardGame.dart';

class ScreenGame extends StatefulWidget {
  @override
  State<ScreenGame> createState() => gameState();
}

class gameState extends State<ScreenGame> {
  List<Question> questionList = getQuestions();
  int currentQuestionIndex = 0;
  int score = 0;
  Answer? selecteAnswer;
  Timer? countdownTimer;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    startTimer();
  }

  void startTimer() {
    countdownTimer =
        Timer.periodic(Duration(seconds: 1), (_) => setCountDown());
  }

  Duration myDuration = Duration(seconds: 10);
  void stopTimer() {
    setState(() => countdownTimer!.cancel());
  }

  void resetTimer() {
    stopTimer();
    setState(() => myDuration = Duration(seconds: 10));
  }

  void setCountDown() {
    final reduceSecondsBy = 1;
    setState(() {
      final seconds = myDuration.inSeconds - reduceSecondsBy;
      if (seconds < 0) {
        countdownTimer!.cancel();
        _next();
      } else {
        myDuration = Duration(seconds: seconds);
      }
    });
  }

  void _next() {
    bool isLastQuestion = false;
    if (currentQuestionIndex == questionList.length - 1) {
      isLastQuestion = true;
    }
    resetTimer();
    if (isLastQuestion) {
      showDialog(context: context, builder: (_) => _showScoreDialog());
    } else {
      setState(() {
        selecteAnswer = null;
        currentQuestionIndex++;
        startTimer();
      });
    }
  }

  _showScoreDialog() {
    final NumQuizCounting numQuiz = Provider.of<NumQuizCounting>(context);
    bool isPassed = false;
    if (score >= questionList.length * 0.6) {
      isPassed = true;
    }
    String title = isPassed ? "Passed" : "Failed";
    return AlertDialog(
      title: Text(
        title + " | Score is $score",
        style: TextStyle(color: isPassed ? Colors.green : Colors.red),
      ),
      content: ElevatedButton(
        child: const Text("Restart"),
        onPressed: () {
          Navigator.pop(context);
          setState(() {
            currentQuestionIndex = 0;
            score = 0;
            selecteAnswer = null;
          });
          resetTimer();
          startTimer();
          numQuiz.resetNum();
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    String strDigits(int n) => n.toString().padLeft(2, '0');
    final seconds = strDigits(myDuration.inSeconds.remainder(60));
    final NumQuizCounting numQuiz = Provider.of<NumQuizCounting>(context);
    return WillPopScope(
      onWillPop: () async {
        final shouldPop = await showDialog<bool>(
          context: context,
          builder: (context) {
            return CupertinoAlertDialog(
              title: const Text('Do You Want To Quit The Game?'),
              // actionsAlignment: MainAxisAlignment.spaceBetween,
              actions: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context, true);
                        numQuiz.resetNum();
                      },
                      child: Container(
                        width: 150,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.green[400],
                        ),
                        padding: const EdgeInsets.symmetric(
                            vertical: 5, horizontal: 10),
                        child: const Text(
                          'Yes',
                          style: TextStyle(
                            fontSize: 20.0,
                            color: Colors.white,
                            fontFamily: "Satisfy",
                            fontWeight: FontWeight.w500,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      child: Container(
                        width: 150,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.red[300],
                        ),
                        padding: const EdgeInsets.symmetric(
                            vertical: 5, horizontal: 10),
                        child: const Text(
                          'No',
                          style: TextStyle(
                            fontSize: 20.0,
                            color: Colors.white,
                            fontFamily: "Satisfy",
                            fontWeight: FontWeight.w500,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            );
          },
        );
        return shouldPop!;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text('Math for kids'),
          backgroundColor: Colors.cyan[400],
        ),
        body: Center(
            child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(children: [
              const boardGame(),
              Padding(
                padding: const EdgeInsets.only(top: 0.0, left: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "TIME: $seconds",
                      style: const TextStyle(fontSize: 22, color: Colors.black),
                    ),
                    Text(
                      "SCORE: $score",
                      style: const TextStyle(fontSize: 22, color: Colors.black),
                    ),
                  ],
                ),
              ),
              _questionWidget(),
              _answerList(),
              _nextButton()
            ]),
          ],
        )),
      ),
    );
  }

  _questionWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      mainAxisSize: MainAxisSize.max,
      children: [
        const SizedBox(height: 20),
        Container(
          alignment: Alignment.center,
          width: 350,
          padding: const EdgeInsets.all(32),
          decoration: BoxDecoration(
              color: Colors.amber[100],
              borderRadius: BorderRadius.circular(16)),
          child: Text(
            questionList[currentQuestionIndex].question,
            style: const TextStyle(
              color: Colors.black,
              fontSize: 36,
              fontWeight: FontWeight.w600,
            ),
          ),
        )
      ],
    );
  }

  _answerList() {
    return Column(
      children: questionList[currentQuestionIndex]
          .answerList
          .map(
            (e) => _answerButton(e),
          )
          .toList(),
    );
  }

  Widget _answerButton(Answer answer) {
    bool isSelected = answer == selecteAnswer;

    return Container(
      width: 300,
      margin: const EdgeInsets.symmetric(vertical: 9),
      height: 78,
      child: ElevatedButton(
        child: Text(
          answer.answer,
          style: TextStyle(fontSize: 38),
        ),
        style: ElevatedButton.styleFrom(
          shape: const StadiumBorder(),
          primary: isSelected ? Colors.blue[200] : Colors.blue[100],
          onPrimary: isSelected ? Colors.white : Colors.black,
        ),
        onPressed: () {
          if (selecteAnswer == null) {
            if (answer.isCorrect) {
              score++;
            }
            setState(() {
              selecteAnswer = answer;
            });
          }
        },
      ),
    );
  }

  _nextButton() {
    final NumQuizCounting numQuiz = Provider.of<NumQuizCounting>(context);
    bool isLastQuestion = false;
    if (currentQuestionIndex == questionList.length - 1) {
      isLastQuestion = true;
    }
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 9),
      width: MediaQuery.of(context).size.width * 0.5,
      height: 48,
      child: ElevatedButton(
        child: Text(isLastQuestion ? "End" : "Next"),
        style: ElevatedButton.styleFrom(
          shape: const StadiumBorder(),
          primary: Colors.yellow[800],
          onPrimary: Colors.white,
        ),
        onPressed: () {
          _next();
          numQuiz.counting();
        },
      ),
    );
  }
}
